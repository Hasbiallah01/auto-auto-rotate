<i>Auto Auto-Rotate</i> is an extremely simple app that stores Android's auto-rotate setting per app.

Do you usually disable auto-rotate and enabled it only for some apps? Like your gallery, or your video app? Do you tend to forget to turn it off afterwards? Does it bother you?

Then <i>Auto Auto-Rotate</i> is the perfect app for you!

<i>Auto Auto-Rotate</i> is open source and released under the GPLv3 license[1]. Check out the code if you like[2].

<b>Required Android Permissions</b>

▸ WRITE_SETTINGS to turn Android's auto-rotate on and off.
▸ BIND_ACCESSIBILITY_SERVICE to detect app launches.
▸ REQUEST_IGNORE_BATTERY_OPTIMIZATIONS to make sure it keeps running in the background.
▸ RECEIVE_BOOT_COMPLETED to automatically startup on boot if activated.
▸ PACKAGE_USAGE_STATS to get the currently running app.

<b>Get it</b>
You can download it for free in F-Droid[3]

<b>Translations</b>

Translations are always welcome! :)

The app is available for translation as two projects on Transifex[4]

[1] https://www.gnu.org/licenses/gpl-3.0.en.html
[2] https://gitlab.com/juanitobananas/auto-auto-rotate
[3] https://f-droid.org/packages/com.jarsilio.android.autoautorotate/
[4] https://www.transifex.com/juanitobananas/auto-auto-rotate and https://www.transifex.com/juanitobananas/libcommon
