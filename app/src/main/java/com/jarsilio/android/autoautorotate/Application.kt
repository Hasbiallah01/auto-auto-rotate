package com.jarsilio.android.autoautorotate

import android.app.Application
import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import com.jarsilio.android.autoautorotate.extensions.isInstalledViaGooglePlay
import com.jarsilio.android.autoautorotate.prefs.Prefs
import com.jarsilio.android.common.logging.LongTagTree
import com.jarsilio.android.common.logging.PersistentTree
import org.acra.ACRA
import org.acra.annotation.AcraCore
import org.acra.annotation.AcraMailSender
import org.acra.annotation.AcraNotification
import org.acra.config.CoreConfigurationBuilder
import org.acra.config.MailSenderConfigurationBuilder
import org.acra.data.StringFormat
import timber.log.Timber

@AcraCore(buildConfigClass = BuildConfig::class)
@AcraMailSender(mailTo = "juam+autoautorotate@posteo.net")
@AcraNotification(
        resTitle = R.string.acra_notification_title,
        resText = R.string.acra_notification_text,
        resChannelName = R.string.acra_notification_channel_name,
        resSendButtonText = R.string.acra_notification_send,
        resDiscardButtonText = android.R.string.cancel,
        resSendButtonIcon = R.drawable.ic_email_gray,
        resDiscardButtonIcon = R.drawable.ic_cancel_gray
)

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        appContext = applicationContext

        Timber.plant(LongTagTree(this))
        Timber.plant(PersistentTree(this))

        AppCompatDelegate.setDefaultNightMode(Prefs.dayNightMode)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)

        if (!BuildConfig.DEBUG) {
            val mailTo = if (isInstalledViaGooglePlay) {
                "juam+autoautorotate-play@posteo.net"
            } else {
                "juam+autoautorotate@posteo.net"
            }
            val builder = CoreConfigurationBuilder(this).apply {
                setBuildConfigClass(BuildConfig::class.java).setReportFormat(StringFormat.JSON)
                getPluginConfigurationBuilder(MailSenderConfigurationBuilder::class.java).setMailTo(mailTo)
            }
            ACRA.init(this, builder)
        }
    }
}

private var appContext: Context? = null

fun requireApplicationContext(): Context {
    return appContext ?: throw IllegalStateException("ApplicationContext should not be null. Did you forget to set it in your Application's onCreate()?")
}
