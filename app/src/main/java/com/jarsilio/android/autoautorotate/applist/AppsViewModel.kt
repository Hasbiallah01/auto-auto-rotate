package com.jarsilio.android.autoautorotate.applist

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel

class AppsViewModel : ViewModel() {
    fun getAutorotateApps(appsDao: AppsDao): LiveData<List<App>> {
        return appsDao.autorotateAppsLive
    }

    fun getNoAutorotateApps(appsDao: AppsDao): LiveData<List<App>> {
        return appsDao.noAutorotateAppsLive
    }
}
