<i>Auto Auto-Rotate</i> एक बेहद सरल ऐप है जो एंड्रॉइड की ऑटो-रोटेट सेटिंग प्रति ऐप स्टोर करता है।

क्या आप आमतौर पर ऑटो-रोटेट को अक्षम करते हैं और इसे केवल कुछ ऐप्स के लिए सक्षम करते हैं? अपनी गैलरी, या अपने वीडियो एप्लिकेशन की तरह? क्या आप इसे बाद में बंद करने के लिए भूल जाते हैं? क्या यह आपको परेशान करता है?

फिर <i>Auto Auto-Rotate</i> आपके लिए एकदम सही ऐप है!

<i>Auto Auto-Rotate</i> ओपन सोर्स है और GPLv3 लाइसेंस [1] के तहत जारी किया गया है। यदि आपको पसंद है तो कोड देखें [2]।

<b>आवश्यक एंड्रॉयड अनुमतियां</b>

▸ WRITE_SETTINGS एंड्रॉयड के ऑटो को ऑन और ऑफ घुमाने की।
ऐप लॉन्च का पता लगाने के लिए ▸ BIND_ACCESSIBILITY_SERVICE।
▸ REQUEST_IGNORE_BATTERY_OPTIMIZATIONS सुनिश्चित करें कि यह पृष्ठभूमि में चल रहा रहता है ।
▸ RECEIVE_BOOT_COMPLETED यदि सक्रिय हो तो बूट पर स्वचालित रूप से स्टार्टअप करें।
▸ PACKAGE_USAGE_STATS वर्तमान में चल रहे ऐप को प्राप्त करना है।

<b>इसे प्राप्त करें</b>
आप इसे एफ-ड्रॉइड में मुफ्त में डाउनलोड कर सकते हैं [3]

<b>अनुवाद</b>

अनुवाद हमेशा स्वागत है! :)

ऐप ट्रांसिफेक्स पर दो परियोजनाओं के रूप में अनुवाद के लिए उपलब्ध है [4]

[1] https://www.gnu.org/licenses/gpl-3.0.en.html
[2] https://gitlab.com/juanitobananas/auto-auto-rotate
[3] https://f-droid.org/packages/com.jarsilio.android.autoautorotate/
[4] https://www.transifex.com/juanitobananas/auto-auto-rotate और https://www.transifex.com/juanitobananas/libcommon