New in version 0.10

★ Make Accessibility Service optional. Some devices
  and Android versions seem to have some issues with it.

New in version 0.9

★ Use Accessibility Service instead of Usage Access:
  more battery friendly and precise recognition of
  running apps.
★ Add dontkillmyapp.com slide (AutoStarter) to AppIntro
  to guide users of some vendors to the configuration
  screens to non-AOSP disable battery optimizations.
