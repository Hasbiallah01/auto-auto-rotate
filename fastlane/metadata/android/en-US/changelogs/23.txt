New in version 0.11.1

★ Update some translations.
★ Upgrade some dependencies.

New in version 0.11

★ Add light/dark mode option (for all Android devices).

New in version 0.10.8

★ Add Indonesian translation. Thank you very much for that, Putu!
★ Update German translation.

New in version 0.10.7

★ Fix small app detection bug while switchting foreground app on some Androids (and using Accessibility Services).
★ Update some translations.


